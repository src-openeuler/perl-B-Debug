Name:      perl-B-Debug
Version:   1.26
Release:   8
Summary:   Walk Perl syntax tree, printing debug info about ops
License:   GPL-1.0-or-later or Artistic-1.0-perl
URL:       https://metacpan.org/release/B-Debug
Source0:   https://cpan.metacpan.org/authors/id/R/RU/RURBAN/B-Debug-%{version}.tar.gz

BuildArch:     noarch

BuildRequires: gcc perl(ExtUtils::MakeMaker) perl-interpreter perl-generators

#for test
BuildRequires: perl(Test::More) perl(Test::Pod) >= 1.00 perl(deprecate)

%description
See ext/B/README and the newer B::Concise.

%package_help

%prep
%autosetup -n B-Debug-%{version} -p1

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PACKLIST=1
%make_build

%install
make pure_install DESTDIR=%{buildroot}
%{_fixperms} %{buildroot}

%check
make test TEST_VERBOSE=1

%files
%doc README
%license Copying Artistic
%{perl_vendorlib}/B/*

%files help
%doc Changes
%{_mandir}/man3/*


%changelog
* Tue Jan 14 2025 Funda Wang <fundawang@yeah.net> - 1.26-8
- drop useless perl(:MODULE_COMPAT) requirement

* Tue Aug 13 2024 gengqihu <gengqihu2@h-partners.com> - 1.26-7
- License info rectification

* Wed Oct 26 2022 renhongxun <renhongxun@h-partners.com> - 1.26-6
- Rebuild for next release

* Wed Jan 05 2022 tianwei<tianwei12@huawei.com> - 1.26-5
- fix test failed

* Fri Sep 27 2019 shenyangyang<shenyangyang4@huawei.com> - 1.26-4
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:move license file

* Wed Aug 28 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.26-3
- Package init
